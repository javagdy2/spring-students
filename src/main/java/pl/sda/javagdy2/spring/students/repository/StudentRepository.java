package pl.sda.javagdy2.spring.students.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.javagdy2.spring.students.model.Student;

import java.util.List;

// klasa która działa jak EntityDao.
// pierwszy typ generyczny, to rodzaj obiektu którym ma nasze repository zarządzać
// drugi typ generyczny to typ identyfikatora naszego modelu
public interface StudentRepository extends JpaRepository<Student, Long> {

    // select * from students where nazwisko = szukaneNazwisko
    List<Student> findAllByNazwisko(String szukaneNazwisko);
//    List<Student> findAllById(List<Long> ids);
}
