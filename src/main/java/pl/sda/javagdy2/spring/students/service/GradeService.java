package pl.sda.javagdy2.spring.students.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.javagdy2.spring.students.controller.StudentController;
import pl.sda.javagdy2.spring.students.model.Grade;
import pl.sda.javagdy2.spring.students.model.Student;
import pl.sda.javagdy2.spring.students.repository.GradeRepository;
import pl.sda.javagdy2.spring.students.repository.StudentRepository;

import java.util.Optional;

@Service
@AllArgsConstructor
public class GradeService {

    // gdy napiszę nad polem autowired to pole zostanie przypisane setterem
    // brak autowired = brak wartości (null)
    // jeśli pole jest final, to musi mieć przypisaną wartość po stworzeniu
    // żeby poprawnie działało, używam adnotacji do konstruktora (AllArgsConstructor)
    private final GradeRepository gradeRepository;
    private final StudentRepository studentRepository;

    // NIE NIE NIE NIE NEI NIE NIE NIE NEI NI E !!!!!111111
    private final StudentController studentController; // NIE NIE NIE NIE NEI NIE NIE NIE NEI NI E !!!!!111111
    // NIE NIE NIE NIE NEI NIE NIE NIE NEI NI E !!!!!111111

    public void saveGrade(Grade grade, Long studentId){
        Optional<Student> studentOptional = studentRepository.findById(studentId);
        if(studentOptional.isPresent()){
            Student student = studentOptional.get();

            grade.setStudent(student);

            gradeRepository.save(grade);
        }
    }

    public Optional<Grade> findById(Long gradeId) {
        return gradeRepository.findById(gradeId); // select * from grade where id = gradeId

    }
}
